// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD0wG49MmsyVYFl-J7Z65fnPOH02kh07JE",
    authDomain: "super-base09.firebaseapp.com",
    databaseURL: "https://super-base09.firebaseio.com",
    projectId: "super-base09",
    storageBucket: "super-base09.appspot.com",
    messagingSenderId: "192376462115",
    appId: "1:192376462115:web:d8b71641758e0200ff4a96",
    measurementId: "G-C4DJWZ5JDL"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//firebase.analytics();
//const database = firebase.database();


const dbRef = firebase.database().ref();

const articlesRef = dbRef.child("articles");

function readArticle(){

    let main = document.querySelector("main");

    articlesRef.on("value", snap => {

        snap.forEach(childSnap =>{

            let art = childSnap.val();

            let $article = document.createElement("article");
            $article.setAttribute("class", "card");
            main.append($article);



            let $header = document.createElement("header");
            $header.setAttribute("class", "card-header card-header-avatar");
            $article.append($header);
            
            let $avatar = document.createElement("img");
            $avatar.setAttribute("class", "card-avatar");
            $avatar.setAttribute("src", "img/avatar.png");
            $avatar.setAttribute("width", "45");
            $avatar.setAttribute("height", "45");
            $header.append($avatar);

            let $h2 = document.createElement("h2");
            $h2.setAttribute("class", "card-title");
            $h2.innerHTML = art.title;
            $header.append($h2);

            let $date = document.createElement("div");
            $date.setAttribute("class", "card-date");
            $date.innerHTML = art.date;
            $header.append($date);



            let $divPI = document.createElement("div");
            $divPI.setAttribute("class", "card-body");
            $article.append($divPI);

            let $pImg = document.createElement("p");
            let $img = document.createElement("img");
            $pImg.append($img);

            $img.setAttribute("src", "img/image.jpg");
            $img.setAttribute("class", "fullwidth")
            $divPI.append($img);

            let $p = document.createElement("p");
            $p.innerHTML = art.content;
            $divPI.append($p);


            main.append($article);
        })      
    })
}

readArticle();

let saveBtn = document.getElementById("save-article-btn");
saveBtn.addEventListener("click", newArticle);

function newArticle(){
    
    let articlesRef = dbRef.child("articles");
    let inputs = document.getElementsByClassName("edit-user-input");
    let newArticle = {};

    for(let i=0 ; i<inputs.length ; i++){
        let key = inputs[i].getAttribute("data-key");
        let value = inputs[i].value;

        newArticle[key] = value;
        newArticle.image = "img/image.jpg";
        newArticle.date = Date.now();

        inputs[i].value = "";
    }
    articlesRef.push(newArticle);

}

